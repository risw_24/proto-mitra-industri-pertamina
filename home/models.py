from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self,email,password,zipcode,institution_name,shipping_address):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(
            email = self.normalize_email(email),
            zipcode= zipcode,
            institution_name = institution_name,
            shipping_address = shipping_address
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
        
class User(AbstractBaseUser):
    email = models.EmailField(max_length=255,unique=True)
    zipcode = models.IntegerField()
    institution_name = models.CharField(max_length=50)
    shipping_address = models.CharField(max_length=100)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()
