from django.http import response
from .forms import *
from django.shortcuts import render,redirect
from django.contrib.auth import login, authenticate, logout
from .models import User

# Create your views here.
def welcome(request):
    return render(request, 'home.html')

def sign_up(request):
    if (request.method == 'POST'):
        form = SignUpForm(request.POST)
        if (form.is_valid()):
            user = form.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(email=user.email, password=raw_password)
            login(request, user)
            return redirect('pemesanan:pemesanan')
    return render(request, 'sign_up.html')

def sign_in(request):
    response = {}
    if (request.method == 'POST'):
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(email=email, password=password)
        if user is not None:
            login(request, user)
            request.session['email'] = user.email
            return redirect('pemesanan:pemesanan')
        else:
            response['message'] = 'Username / Password salah'
    return render(request, 'sign_in.html')

def sign_out(request):
    logout(request)
    return redirect('home:welcome')