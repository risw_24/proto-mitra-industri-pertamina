from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from .models import *

class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['zipcode', 'institution_name', 'shipping_address','email']