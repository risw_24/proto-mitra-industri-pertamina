from django.db.models import fields
from django.forms.models import ModelForm
from pemesanan.models import Bbm
from django import forms

class BbmForm(ModelForm):
    class Meta:
        model = Bbm
        fields = ['lokasi', 'jumlah', 'tanggal_pengiriman', 'transportir']
        exclude = ('user',)