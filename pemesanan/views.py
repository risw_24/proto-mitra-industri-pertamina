from django.shortcuts import render, redirect
from .forms import *
from .models import *

# Create your views here.
def pemesanan(request):
    if 'email' not in request.session:
        return redirect('home:sign_in')
    
    if(request.method=="POST"):
        form = BbmForm(request.POST)
        form.instance.user = request.user
        if form.is_valid():
            form.save()
            return redirect('pemesanan:lihatPemesanan')
        else:
            print("gagal")
            form = Bbm()
        
        context = {
            'form': form
            }

    return render(request, 'pemesanan.html')

def lihatPemesanan(request):
    if 'email' not in request.session:
        return redirect('home:sign_in')

    bbm = Bbm.objects.filter(user=request.user)
    context ={
			'bbm' : bbm
		}
    return render(request, 'lihatPemesanan.html', context)