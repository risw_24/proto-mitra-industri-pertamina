from django.contrib import admin
from django.urls import path
from . import views
app_name='pemesanan'

urlpatterns = [
    path('', views.pemesanan, name='pemesanan'),
    path('lihatPemesanan/', views.lihatPemesanan, name='lihatPemesanan'),
]