from django.db import models
from django.db.models.fields import IntegerField
from pertamina.settings import AUTH_USER_MODEL as User
# Create your models here.

class Bbm(models.Model):
    id = models.AutoField(primary_key=True)
    lokasi = models.CharField(max_length=100)
    jumlah = models.IntegerField()
    tanggal_pengiriman = models.DateField()
    transportir = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)